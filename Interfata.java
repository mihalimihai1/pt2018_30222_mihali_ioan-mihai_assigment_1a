package Interface;

import javax.swing.*;


public class Interfata {
	JLabel l1, l2, l3;
	JPanel panou;
	JTextField t1,t2,t3;
	JButton b1,b2, b3, b4, b5, b6, b7;
	
	public Interfata() {
		
		JFrame frame = new JFrame("Polynomials");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(500, 300);
		panou = new JPanel();
		panou.setLayout(null);
		l1 = new JLabel("Baga pol1: ");
		l2 = new JLabel("Baga pol2: ");
		l3=new JLabel("Rezultat: ");
		
		t1=new JTextField("");
		t2=new JTextField("");
		t3=new JTextField("");
		
		b1=new JButton("Adunare");
		b2=new JButton("Scadere");
		b3=new JButton("Inmultire");
		b4=new JButton("Derivare Pol1");
		b5=new JButton("Integrare Pol1");
		b6=new JButton("Derivare Pol2");
		b7=new JButton("Integrare Pol2");
		
		frame.add(panou);
		frame.setBounds(150,100,600,400);
		//Pol1
		l1.setBounds(10,15,100,30);  //x y lungime latime
		panou.add(l1);
		t1.setBounds(110,15,200,30);
		panou.add(t1);
		//Pol2
		l2.setBounds(10,55,100,30);
		panou.add(l2);
		t2.setBounds(110,55,200,30);
		panou.add(t2);
		//Rezultat
		l3.setBounds(15,100,100,30);
		panou.add(l3);
		t3.setBounds(110,100,400,30);
		panou.add(t3);
		//Operatiile
		b1.setBounds(20,200,120,35);
		panou.add(b1);
		b2.setBounds(20,300,120,35);
		panou.add(b2);
		b3.setBounds(20,250,120,35);
		panou.add(b3);
		b4.setBounds(140,250,120,35);
		panou.add(b4);
		b5.setBounds(260,250,120,35);
		panou.add(b5);
		b6.setBounds(140,300,120,35);
		panou.add(b6);
		b7.setBounds(260,300,120,35);
		panou.add(b7);
		frame.setVisible(true);
		
	}
	void setTotal(String polynomm)
	{
		t3.setText(polynomm);
	}
	public void showError(String string) {
		// TODO Auto-generated method stub
		
	}
}
