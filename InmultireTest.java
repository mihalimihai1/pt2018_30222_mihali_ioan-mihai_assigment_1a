package Servicii;

import Polinoame.Monom;
import Polinoame.Polinom;
import junit.framework.TestCase;

public class InmultireTest extends TestCase {
	
	public void testFailure() {
		Monom m1 = new Monom(1,1);
		Monom m2 = new Monom(2,2);
	    Monom m3 = new Monom(3,2);
		Polinom a = new Polinom();
		Polinom b = new Polinom();
		Polinom rez;
		a.adaugaMonom(m1);
		a.adaugaMonom(m2);
		b.adaugaMonom(m3);
		rez=Operatii.inmultire(a,b);
		assertEquals(" +6.0X^4  +3.0X^3 ", rez.toString());
		}

}
