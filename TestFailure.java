package Servicii;

import Polinoame.Monom;
import Polinoame.Polinom;
import junit.framework.TestCase;

public class TestFailure extends TestCase{
	
	//asta ii adunarea
	public void testFailure() {
		Monom m1 = new Monom(1,1);
		Monom m2 = new Monom(2,2);
	    Monom m3 = new Monom(3,2);
		Polinom a = new Polinom();
		Polinom b = new Polinom();
		Polinom rez;
		a.adaugaMonom(m1);
		a.adaugaMonom(m2);
		b.adaugaMonom(m3);
		rez=Operatii.adunare(a,b);
		String s=rez.toString();
		assertEquals(" +5.0X^2  +1.0X^1 ", s);  
	}
}
