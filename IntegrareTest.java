package Servicii;

import Polinoame.Monom;
import Polinoame.Polinom;
import junit.framework.TestCase;

public class IntegrareTest extends TestCase {
	public void testFailure() {
		Monom m1 = new Monom(6,2);
		Monom m2 = new Monom(2,1);
		Polinom a = new Polinom();
		Polinom rez;
		a.adaugaMonom(m1);
		a.adaugaMonom(m2);
		rez=Operatii.integrare(a);
		assertEquals(" +2.0X^3  +1.0X^2 ", rez.toString());
		}

}
