package Servicii;

import Polinoame.Monom;
import Polinoame.Polinom;
import junit.framework.TestCase;

public class DerivareTest extends TestCase {
	public void testFailure() {
		Monom m1 = new Monom(1,1);
		Monom m2 = new Monom(2,2);
		Polinom a = new Polinom();
		Polinom rez;
		a.adaugaMonom(m1);
		a.adaugaMonom(m2);
		rez=Operatii.derivare(a);
		assertEquals(" +4.0X^1  +1.0X^0 ", rez.toString());
		}

}
