package Polinoame;
import java.awt.List;
import java.util.ArrayList;

public class Polinom {
	public ArrayList<Monom> Monoame;
	public double max=0;
	public int v=0;
	public String[] a= new String[200];
	
	public Polinom() {
		this.Monoame =new ArrayList<Monom>();
	}
	
	public void adaugaMonom(Monom nM) {
		boolean ok = false;
		for (Monom m : Monoame) {
			if (m.putere == nM.putere) {
				m.coef += nM.coef;
				ok = true;
				break;
			}
		}
		if (ok == false) {
			Monoame.add(nM);
		}
	}
	
	public Polinom copy(){
		Polinom p = new Polinom();
		for(Monom m: Monoame)
			if(m.coef!=0){
			p.adaugaMonom(new Monom(m.coef, m.putere));}
		return p;
	}
	
	public String toString() 
	{
		String y="";
		for ( Monom i:Monoame ){
			if(i.putere>max){
				max=i.putere;
				v++;
				for(int k=v;k>=1;k--){
					a[k+1]=a[k];
				}
				a[1]=i.toString();
			}
			else{
			v++;
			a[v]=i.toString();
			}
		}
		for(int k=1;k<=v;k++){
		y=y+" "+a[k]+" ";}	
		return y;
	}
}